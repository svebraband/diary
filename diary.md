# 24-01-04

## Who/Where
* Where: Alice's Place
* Who: Björn, Gabriel, Lucas and Matti


## Talks about the future
* All the talks were just initial talks, and as we don't have Hanna nothing is
complete defined
* We probably won't be able to have a gig on January as there are few meetings
* Meetings: around 1 per week
* Try to agree before rehearsal on what we are going to work
* "Acoustic" or "electrified" (+ drumm set) band: Try both, going more acoustic
now as it is easier and more portable.
* Covers or composing songs: Try both, focusing more at covers for now as it 
is more fluid and we can know each other musical taste better

## Songs we worked
* I am a scientist - Guided by Voices : https://open.spotify.com/track/0oN2fJx5t5BzkKQQiojHrr
* Gabriel's song

## Suggestions of song to play
* Glory box - Portishead : https://open.spotify.com/track/3Ty7OTBNSigGEpeW2PqcsC
* Frame by Frame -  King Crimson : https://open.spotify.com/track/0yg93GXlS0ZmLsFpXG5bT2

## Records
* https://archive.org/details/iam-ascientist-240104



# 24-01-11

## Who/Where
* Where: Alice's Place
* Who: Björn, Gabriel, Lucas, Matti and Hanna

## Songs we worked
* Gabriel's song
* Det lyser en stjärna
* ?

## Records
* https://archive.org/details/meeting-24-01-11

# 24-01-16

## Who/Where
* Where: Alice's Place
* Who: Björn, Gabriel, Lucas, Matti and Hanna

## Songs we worked
* Gabriel's song
* Det lyser en stjärna
* 16 tons

## Records

https://archive.org/details/meeting-24-01-16

## Lyrics for Gabrie's Song (Svenkt vinter Samba?)

```
Você chegou para me dizer
Que queria conhecer
Um pouco mais
Mas assim não dá
Assim não dá pra ser

O dia foi, a noite chegou
E assim vai ficar
Até você perceber que não dá pra ser
Não dá pra ser

O som que faz
O inverno que chega
Um samba que cai
Nem um samba se faz
Num inverno sem paz
```

# 24-01-20 - Celebration with Feijoada

## Who/Where
* Where: Gabriel's Place
* Who:Gabriel, Lucas, Matti and Hanna

## Setlist
* These shoes are made for walking
* Det lyser en stjärna
* Way side
* Gabriel's song
* 16 tons

